from flask import Flask,render_template
import os
import re

app = Flask(__name__)

@app.route('/')
def hello():
    return"UOCIS docker demo!"

@app.route('/<name>/')
def file(name):
	if (os.path.isfile (name))==True:
		return render_template(name),200

	elif "~" in name:
		return render_template('403.html')
	elif "//" in name:
		return render_template('403.html')

	elif ".." in name:
		return render_template('403.html')

	else:
		return render_template('404.html')

@app.route('/<name>')
def file2(name):
	if (os.path.isfile (name))==True:
		return render_template(name),200

	elif "~" in name:
		return render_template('403.html')
	elif "//" in name:
		return render_template('403.html')

	elif ".." in name:
		return render_template('403.html')

	else:
		return render_template('404.html')

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
