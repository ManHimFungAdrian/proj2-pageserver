A "getting started" manual for Dockers. CIS 322, Introduction to Software Engineering, at the University of Oregon. Reference: https://docs.docker.com/engine/reference/builder/


# Getting started on the flask project

* Go to the web folder in the repository. Read every line of the docker file and the simple flask app.

* Build the simple flask app image using

  ```
  docker build -t UOCIS-flask-demo .
  ```
  
* Run the container using
  
  ```
  docker run -d -p 5000:5000 UOCIS-flask-demo
  ```

* Launch http://127.0.0.1:5000 using web broweser and check the output "UOCIS docker demo!"
  
